grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat )+ EOF!;

stat
    : expr NL -> expr

//    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | PRINT expr -> ^(PRINT expr)
    | START_SCOPE
    | END_SCOPE
    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)

    | NL ->
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atomExpr
      ( MUL^ atomExpr
      | DIV^ atomExpr
      | MOD^ atomExpr
      )*
    ;
    
atomExpr
    : atom
      ( EXP^ atom
      | SQRT^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

VAR :'var';

PRINT: 'print';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;

START_SCOPE
  : '{'
  ;

  

END_SCOPE
  : '}'
  ;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
	
MOD
  : '%'
  ;

EXP
  : '^'
  ;

SQRT
  : '#'
  ;
