tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (expr)* ;

expr returns [Integer out]
	        : INT {$out = getInt($INT.text);}
	        | (name=ID) {$out = symbols.getSymbol($name.text);}
		      | ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;}
        | ^(PODST name=ID   e2=expr) {symbols.setSymbol($name.text, $e2.out);}
        | ^(EXP   e1=expr e2=expr) {$out = calculatePower($e1.out, $e2.out);}
        | ^(SQRT   e1=expr e2=expr) {$out = calculateSqrt($e1.out, $e2.out);}
        | ^(MOD   e1=expr e2=expr) {$out = calculateModulo($e1.out, $e2.out);}
        | ^(PRINT e1=expr) {print($e1.out);}
        | ^(VAR name=ID) {symbols.newSymbol($name.text);}
        | START_SCOPE {symbols.enterScope();}
        | END_SCOPE {symbols.leaveScope();}
        ;
        
catch [Exception e] {
        System.out.println(e.getMessage());

}