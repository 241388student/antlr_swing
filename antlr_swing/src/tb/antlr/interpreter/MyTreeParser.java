package tb.antlr.interpreter;

import java.util.Optional;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.LocalSymbols;

public class MyTreeParser extends TreeParser {
	
	protected LocalSymbols symbols = new LocalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }
    
    protected int calculateSqrt(int number, int degree) {

    	return (int) Math.pow(number, 1.0 / degree);

    }
    

    protected int calculatePower(int number, int degree) {

    	return (int) Math.pow(number, degree);

    }

    

    protected int calculateModulo(int a, int b) {

    	return a % b;

    }

    

    protected void print(Object o) {

    	Optional<Object> opt = Optional.ofNullable(o);

    	if(opt.isPresent()) {

        	System.out.println(opt.get().toString());

    	}
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
}
